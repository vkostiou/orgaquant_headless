# Introduction

This repository contains the source code for running OrgaQuant tool in headless mode.
OrgaQuant's publication can be found [here](https://www.nature.com/articles/s41598-019-48874-y).

# How to run

## Option 1: Use the docker container

### Requirements

- [Docker engine](https://docs.docker.com/get-docker/)

### Run in a single step

    $ INPUT_DIR=/absolute/path/to/dir/containing/input/stack
    $ WORK_DIR=/absolute/path/to/your/preferred/results/dir
    $ docker run --mount type=bind,source=$INPUT_DIR,target=/input_stack_dir --mount type=bind,source=$WORK_DIR,target=/workdir registry.gitlab.com/vkostiou/orgaquant_headless conda run -n oq python orgaquant_headless.py -i input_stack_dir/MYSTACK.czi -w /workdir

### or Run interactively (displays progress bar)

    $ INPUT_DIR=/absolute/path/to/dir/containing/stack/slice/images
    $ WORK_DIR=/absolute/path/to/your/preferred/results/dir
    $ docker run -it --mount type=bind,source=$INPUT_DIR,target=/input_stack_dir --mount type=bind,source=$WORK_DIR,target=/workdir registry.gitlab.com/vkostiou/orgaquant_headless
    
The above command allows for entering the container and running interactive commands, as below:

    $ conda activate oq
    $ python orgaquant_headless.py -i /input_stack_dir/MYSTACK.czi -w /workdir



## Option 2: Install dependencies and run the Python script

>It is strongly recommended to create a virtual environment using **Python 3.6** before installing the required packages.

    $ pip install requirements.txt

# Usage

    usage: orgaquant_headless.py [-h] -i PATH -w PATH [-m PATH]
                                 [-s SINGLE VALUE or RANGE]    
                                 [-c SINGLE VALUE or RANGE]
                                 [-t SINGLE VALUE or RANGE]
    
    This script allows running OrgaQuant in headless mode.
    It takes a stack file as input and the analysis working directory containing the set of stack slice images.
    Each OrgaQuant parameter is optional and can be passed either as a single value or
    an inclusive range (min,max,step) which allows for parameter sweeping.
    E.g. orgaquant_headless.py -i "input_stack_file" -w "path/to/analysis/working/directory" -s 1300,1700,100 -c 1.5 -t 0.8,0.9,0.05
    
    optional arguments:
      -h, --help            show this help message and exit
      -m PATH, --model PATH
                            Set model path, default:
                            trained_models/orgaquant_intestinal_v3.h5
      -s SINGLE VALUE or RANGE, --image_size SINGLE VALUE or RANGE
                            Set image size parameter, min: 800, max: 2000,
                            default: 1200
      -c SINGLE VALUE or RANGE, --contrast SINGLE VALUE or RANGE
                            Set contrast parameter, min: 1.0, max: 3.0, default:
                            1.75
      -t SINGLE VALUE or RANGE, --confidence_threshold SINGLE VALUE or RANGE
                            Set confidence threshold parameter, min: 0, max: 1,
                            default: 0.85
    
    Required named arguments:
      -i PATH, --input_stack PATH
                            Input stack file
      -w PATH, --workdir PATH
                            Analysis working directory (it contains your stack
                            slice images)


# Output

Output files can be found in a newly created directory named *orgaquant_output*, inside the input directory provided by the user.