import argparse
import logging
import os
import sys
import textwrap

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm

from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image, \
    adjust_contrast
from keras_retinanet.utils.visualization import draw_box

IMAGE_SIZE_MIN = 800
IMAGE_SIZE_MAX = 2000
IMAGE_SIZE_DEFAULT = 1200
CONTRAST_MIN = 1.0
CONTRAST_MAX = 3.0
CONTRAST_DEFAULT = 1.75
CONFIDENCE_THRESHOLD_MIN = 0
CONFIDENCE_THRESHOLD_MAX = 1
CONFIDENCE_THRESHOLD_DEFAULT = 0.85

def configure_logger(logger):
    logger.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    log_format = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(module)s:%(funcName)s - %(message)s')
    console_handler.setFormatter(log_format)

    logger.addHandler(console_handler)


logger = logging.getLogger("logger")
configure_logger(logger)


class Orgaquant():
    def __init__(self,
                 model_path=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'trained_models',
                                    'orgaquant_intestinal_v3.h5')):
        self.model = models.load_model(model_path)

    def set_input_image_dir(self, path):
        if os.path.exists(path):
            self.input_image_dir = path
        else:
            logger.error(f'Path {path} does not exist!')
            sys.exit(2)

    def __load_bright_input_images__(self):
        imagelist = []
        for root, directories, filenames in os.walk(self.input_image_dir):
            if 'Bright' in root and 'orgaquant_output' not in root:
                imagelist = imagelist + [os.path.join(root, x) for x in filenames if
                                         x.endswith(('.jpg', '.tif', '.TIF', '.png', '.jpeg', '.tiff'))]
        return imagelist

    def set_input_parameters(self, image_size, contrast, confidence_threshold):
        self.image_size = image_size
        self.contrast = contrast
        self.confidence_threshold = confidence_threshold
        info_msg = f"""Detecting organoids for these parameters:
        Image size: {image_size}
        Contrast: {contrast}
        Confidence threshold: {confidence_threshold}"""
        logger.info(info_msg)


    def run(self):
        imagelist = self.__load_bright_input_images__()
        for i, filename in enumerate(tqdm(imagelist)):
            try:
                # load image
                image = read_image_bgr(filename)

                # copy to draw on
                draw = image.copy()
                draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

                # preprocess image for network
                image = adjust_contrast(image, self.contrast)
                image = preprocess_image(image)
                image, scale = resize_image(image, min_side=self.image_size, max_side=2048)

                # process image
                boxes, scores, labels = self.model.predict_on_batch(np.expand_dims(image, axis=0))

                # correct for image scale
                boxes /= scale

                out = np.empty((0, 4), dtype=np.float32)

                # visualize detections
                for box, score, label in zip(boxes[0], scores[0], labels[0]):
                    # scores are sorted so we can break
                    if score < self.confidence_threshold:
                        break
                    out = np.append(out, box.reshape(1, 4), axis=0)

                    b = box.astype(int)
                    draw_box(draw, b, color=(255, 0, 255))

                self.__save_output__(draw, filename, out)
            except:
                pass

    def __save_output__(self, draw, filename, out):
        output = pd.DataFrame(out, columns=['x1', 'y1', 'x2', 'y2'], dtype=np.int16)
        output['Diameter 1 (Pixels)'] = output['x2'] - output['x1']
        output['Diameter 2 (Pixels)'] = output['y2'] - output['y1']
        image_name = os.path.basename(filename)
        image_dir = os.path.dirname(filename)
        output_dir = f'{self.image_size}_{self.contrast}_{self.confidence_threshold}'
        try:
            os.makedirs(os.path.join(image_dir, 'orgaquant_output', output_dir))
        except FileExistsError:
            pass
        output_name = os.path.join(image_dir, 'orgaquant_output', output_dir, image_name)
        output.to_csv(output_name + '.csv', index=False)
        plt.imsave(output_name + '_detected.png', draw)


def main():
    def validate_arg(f: str) -> str:
        if not os.path.exists(f):
            error_msg = f"{f} does not exist"
            logger.error(error_msg)
            raise argparse.ArgumentTypeError(error_msg)
        return f

    description = textwrap.dedent('''
    This script allows running OrgaQuant in headless mode.
    It takes a stack file as input and the analysis working directory containing the set of stack slice images. 
    Each OrgaQuant parameter is optional and can be passed either as a single value or 
    an inclusive range (min,max,step) which allows for parameter sweeping.
    E.g. orgaquant_headless.py -i "path/to/input/images" -s 1300,1700,100 -c 1.5 -t 0.8,0.9,0.05
    ''')
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=description)
    requiredNamed = parser.add_argument_group('Required named arguments')
    requiredNamed.add_argument('-i', '--input_stack', help='Input stack file',
                               required=True, type=validate_arg, metavar='PATH')
    requiredNamed.add_argument('-w', '--workdir', help='Analysis working directory (it contains your stack slice images)',
                               required=True, type=validate_arg, metavar='PATH')

    parser.add_argument('-m', '--model', type=validate_arg, metavar='PATH',
                        help='Set model path, default: trained_models/orgaquant_intestinal_v3.h5')
    parser.add_argument('-s', '--image_size', type=str, metavar='SINGLE VALUE or RANGE',
                        help=f'Set image size parameter, min: {IMAGE_SIZE_MIN}, max: {IMAGE_SIZE_MAX}, default: {IMAGE_SIZE_DEFAULT}')
    parser.add_argument('-c', '--contrast', type=str, metavar='SINGLE VALUE or RANGE',
                        help=f'Set contrast parameter, min: {CONTRAST_MIN}, max: {CONTRAST_MAX}, default: {CONTRAST_DEFAULT}')
    parser.add_argument('-t', '--confidence_threshold', type=str, metavar='SINGLE VALUE or RANGE',
                        help=f'Set confidence threshold parameter, min: {CONFIDENCE_THRESHOLD_MIN}, max: {CONFIDENCE_THRESHOLD_MAX}, default: {CONFIDENCE_THRESHOLD_DEFAULT}')
    args = parser.parse_args()

    image_sizes = []
    contrasts = []
    confidence_thresholds = []

    def validate_parameter(parameter, parameter_name, limits):
        lower_limit, upper_limit = limits
        parameter_values = []
        if ',' in parameter:
            try:
                min, max, step = map(lambda x: int(float(x) * 100), parameter.split(','))
                parameter_values = [i/100 for i in range(min, max + step, step)]
            except:
                sys.exit(parser.print_usage())
        else:
            parameter_values.append(float(parameter))

        filtered_parameter_values = list(filter(lambda x: lower_limit <= x <= upper_limit, parameter_values))

        if len(filtered_parameter_values) != len(parameter_values):
            logger.warning(f'Warning! Some {parameter_name} values are out of range and will be ignored')

        return filtered_parameter_values

    if args.image_size:
        image_sizes = validate_parameter(args.image_size, 'image_size', (IMAGE_SIZE_MIN, IMAGE_SIZE_MAX))
    else:
        image_sizes.append(IMAGE_SIZE_DEFAULT)

    if args.contrast:
        contrasts = validate_parameter(args.contrast, 'contrast', (CONTRAST_MIN, CONTRAST_MAX))
    else:
        contrasts.append(CONTRAST_DEFAULT)

    if args.confidence_threshold:
        confidence_thresholds = validate_parameter(args.confidence_threshold, 'confidence_threshold', (CONFIDENCE_THRESHOLD_MIN, CONFIDENCE_THRESHOLD_MAX))
    else:
        confidence_thresholds.append(CONFIDENCE_THRESHOLD_DEFAULT)

    number_of_combinations = len(image_sizes) * len(contrasts) * len(confidence_thresholds)
    if number_of_combinations > 1:
        logger.info(f'Number of combinations: {number_of_combinations}')
    logger.info(f'Image sizes: {image_sizes}')
    logger.info(f'Contrasts: {contrasts}')
    logger.info(f'Confidence thresholds: {confidence_thresholds}')

    oq = Orgaquant()
    stack_name = os.path.splitext(os.path.basename(args.input_stack))[0]
    oq.set_input_image_dir(os.path.join(args.workdir, stack_name))
    for image_size in image_sizes:
        for contrast in contrasts:
            for confidence_threshold in confidence_thresholds:
                oq.set_input_parameters(int(image_size), contrast, confidence_threshold)
                oq.run()
    logger.info('Organoid detection completed!')


if __name__ == "__main__":
    main()
